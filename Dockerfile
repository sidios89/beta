FROM maven:3.6.3-adoptopenjdk-15
COPY . .
RUN mvn clean install
ENTRYPOINT java -jar target/beta-0.0.1-SNAPSHOT.jar
